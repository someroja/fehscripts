local GratsMon = CreateFrame("Frame")

function GratsMon:OnEvent(event)
    if (event == "ACHIEVEMENT_EARNED") then
        PlaySound(24260) -- Hey! Grats, mon!
    end

    if (event == "PLAYER_LEVEL_UP") then
        PlaySound(24297) -- Ding!
    end
end

GratsMon:SetScript("OnEvent", GratsMon.OnEvent)
GratsMon:RegisterEvent("ACHIEVEMENT_EARNED")
GratsMon:RegisterEvent("PLAYER_LEVEL_UP")
