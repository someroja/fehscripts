local SellJunk = CreateFrame("Frame")

function SellJunk:Sell()
    for bagID = 0, 4 do
        local slots = GetContainerNumSlots(bagID)
        if slots > 0 then
            for slot = 1, slots do
                local quality, _, _, link = select(4, GetContainerItemInfo(bagID, slot))
                local sellPrice = (link and select(11, GetItemInfo(link))) or 0
                local isJunk = quality == 0
                local canBeSold = sellPrice > 0
                if isJunk and canBeSold and self.merchantAvailable then
                    UseContainerItem(bagID, slot)
                end
            end
        end
    end
end

function SellJunk:Repair()
    if self.merchantAvailable and CanMerchantRepair() then
        local cost, canRepair = GetRepairAllCost()
        if canRepair and cost <= GetMoney() then
            RepairAllItems()
        end
    end
end

function SellJunk:OnEvent(event)
    if (event == "MERCHANT_SHOW") then
        self.merchantAvailable = true
        -- Go through the bags first and sell junk before repairing
        self:Sell()
        self:Repair()
    elseif (event == "MERCHANT_CLOSED") then
        self.merchantAvailable = false
    end
end

SellJunk:SetScript("OnEvent", SellJunk.OnEvent)
SellJunk:RegisterEvent("MERCHANT_SHOW")
SellJunk:RegisterEvent("MERCHANT_CLOSED")
